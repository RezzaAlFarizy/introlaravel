@extends('layout.master')

@section('title')
    Halaman Registrasi
@endsection

@section('content')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First Name : </label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last Name : </label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Gender: </label><br><br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br><br>
        <label for="nationality">Nationality: </label><br><br>
            <select name="national" id="nationality">
                <option value="1">Indonesia</option>
                <option value="2">Malaysia</option>
                <option value="3">Brunei Darussalam</option>
                <option value="3">Other</option>
            </select><br><br>
        <label>Language Spoken: </label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" name="Sign Up">
        
    </form>

@endsection